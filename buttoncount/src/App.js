import React,{useState} from 'react'
import "./App.css"

const App = () => {
  const[count,setCount]=useState(0)
  function increment(){
    setCount(prevCount=>prevCount+1)
  }
  function decrement(){
    setCount(prevCount=>prevCount-1)
  }
  return (
    <div>
      <button onClick={increment}>+</button>
      <span>{count}</span>
      <button onClick={decrement}>-</button>
    </div>
  )
}

export default App
