import React from 'react'
import footer from "./footer"
import header from "./header"

const App = () => {
  return (
    <div>
      <header/>
      <footer/>
    </div>
  )
}

export default App
