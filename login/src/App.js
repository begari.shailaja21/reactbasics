import React,{useState} from 'react'
import ".//App.css"

const App = () => {
  const[data,setData]=useState({
    username:"",
    email:"",
    password:"",
    conformPassword:""
  })
  const {username,email,password,conformPassword}=data;
  const changeHandler=e=>{
    setData({...data,[e.target.name]:e.target.value})
  }
  const submitHandelar=e=>{
    e.preventDefault()
    if(username.length<5){
      alert("username must be alteast 5 characters")
    }else if(password!=conformPassword){
      alert("passwors are not matching!!")
    }else{
      console.log(data)
    }
  }

  return (
    <div>
    <h1>LINKEDIN  <span className="in">in</span></h1>
      <form onSubmit={submitHandelar}>
      <input type="text" name="username" value={username} onChange={changeHandler}  placeholder="username" />
      <input  type="email"  name="email" value={email} onChange={changeHandler} placeholder="enter email.." />
      <input  type="password" name="password" value={password} onChange={changeHandler}  placeholder="password" /> 
      <br></br>
      <input type="password" name="conformPassword" value={conformPassword} onChange={changeHandler} placeholder="conform-password" />
      {password !=conformPassword ? <p style={{"color":"red"}}>Passwords not matching</p> :null}
      <h3>Forget password ?</h3>
      <input type="submit" name="submit" className="submit" />

      </form>
    </div>
  )
}

export default App
