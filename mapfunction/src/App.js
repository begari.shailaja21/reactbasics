import React from 'react'
import ".//App.css"
const App = () => {
  // const array=["React js","Angular js","Javascript","NOde js"]
  const array=[
    {
      id:1,
      titile:"Nodejs"
    },
    {
      id:2,
      titile:"React js"
    }
  ]
  return (
    <div>
      {
        array.map(
          //single array means like this we have to write//
          // (value,index)=><li>{value}</li>
          // array in properties by using this//
          (value,index)=><li key={value.titile}>{value.titile}</li>
        )
      }
    </div>
  )
}

export default App
